import axios from "axios";
const BASE_URL = "https://xkcd.com/";
const getComic = async (idComic) => {
  const rawData = await axios.get(
    `https://cors-anywhere.herokuapp.com/${BASE_URL}${idComic}/info.0.json`
  );
  return rawData
};

export default {
  getComic,
};
