# Bienvenidos a comic-app

Comic app es una aplicación que te muestra un comic aleatorio cuando inicias en ella y te permite ir navegando hacia los siguientes o anteriores comic, además podras seleccionar una calificación para cada comic y agregar un comentario, la app se encuentra en desarrollo a la espera del endpoint para implementar el guardado de calificación y comentarios.

Clone este proyecto en su equipo :

## Iniciar el proyecto
```
Ejecute en su consola npm install
```

### Compile y recargue en modo producción
```
Ejecute en su consola npm run serve
```

### Compile minifique para producción
```
Ejecute en su consola npm run build
```

### Para corregir error de lints
```
Ejecute en su consola npm run lint
```

### Importante
```
La aplicación recupera información de la API https://xkcd.com/614/info.0.json, aunque por motivos de CORS no se puede acceder directamente a este endpoint, motivo por el cual se debe hacer uso en conjunto con https://cors-anywhere.herokuapp.com/ la cual permite utilizar el endpoint anterior en modo de desarrollo, si por algún motivo no le carga la imagen del comic le invito a que entre a la url y habilite temporalmente el uso.
```


